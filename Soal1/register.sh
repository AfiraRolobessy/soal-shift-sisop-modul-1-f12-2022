#!/bin/bash

LOGFILE="./log.txt"
TIMESTAMP=`date "+%m-%d-%y %H:%M:%S"`

echo "enter username"
read uName

if grep -q "$uName " ./users/user.txt
then    
    echo "User is already in file"
    echo "$TIMESTAMP REGISTER: ERROR User already exists" >> $LOGFILE
else
    echo "enter password"
    read -s uPass
    length=${#uPass}
    if grep -q '^[-0-9a-zA-Z]*$' <<<$uPass && [ "$uPass" != "$uName" ] && [ $length -ge 8 ] && [[ $uPass =~ [A-Z] ]] && [[ $uPass =~ [a-z] ]] && [[ $uPass =~ [0-9] ]]
    then
        echo "Register Successful"
        echo "$TIMESTAMP REGISTER: INFO User $uName registered successfully" >> $LOGFILE
        echo "$uName $uPass" >> ./users/user.txt
    else 
        echo -e "\nDemi menjaga keamanan, input password yang didaftarkan memiliki kriteria sebagai berikut:
                Minimal 8 karakter
                Memiliki minimal 1 huruf kapital dan 1 huruf kecil
                Alphanumeric
                Tidak boleh sama dengan username"
    fi
fi
