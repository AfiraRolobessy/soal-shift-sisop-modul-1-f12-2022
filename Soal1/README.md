LAPORAN RESMI

![image.png](./image.png)
![image-1.png](./image-1.png)

#### Cara pengerjaan
    melihat dokumentasi di internet mengenai grep, echo, date, logfile, timestamp, awk, if else, while loop, case, mkdir, wget,
    hidden input, alphanumerical, uppercase, lowercase, comparing 2 string, file access, zip & unzip, bash, sh, nano
    kemudian menyusun kode berdasarkan soal yang diminta

#### Kendala yang dialami
    Ferry: 
        - kode tidak bisa dijalankan menggunakan perintah sh, (menggunakan bash)
        - virtual machine mengalami lag sehingga menggunakan wsl,
        - dokumentasi di internet terkadang tidak dapat dijalankan / error, sehingga harus mencari alternatif lain,
        - saat [CTRL+Z] menggunakan nano, file hilang / kosong, menyita banyak waktu karena harus menulis ulang kode
        - kode tidak bisa dihapus satu persatu, harus diakali menggunakan cut [CTRL+Z].
        - belum memahami secara penuh mengenai akses-akses file
    Nugraha Akbar
        - belum memahami cara unzip file terlebih dahulu jika sudah terdapat file zip dengan nama yang sama.
        - Belum memahami cara loop jika password tidak sesuai dengan ketentuan.
    Afira:
       - belum familiar dengan command/perintah yang ada Linux
       - sulit mendapatkan referensi dalam meneyelesaikan soal yang diberikan
