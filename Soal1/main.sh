#!/bin/bash

LOGFILE="./log.txt"
TIMESTAMP=`date "+%m-%d-%y %H:%M:%S"`

echo "enter username"
read uName

if grep -q "$uName" ./users/user.txt
then
	echo "enter password"
	read -s uPass
	if grep -xq "$uName $uPass" ./users/user.txt
 	then
  		echo "$TIMESTAMP LOGIN: INFO User USERNAME logged in" >> $LOGFILE
  		echo -e "Login Successful\n"

 		date_now=`date "+%Y-%m-%d"`
  		folder_name=$date_now\_$uName

  		read -p "Choose what you want to do: " choice
  		case $choice in
   		"dl")
   			read N
   			for ((num=1; num <=$N; num=num+1))
   			do 
    				value=$(printf "%02d" $num)
    				mkdir -p ./$folder_name/ && wget -O ./$folder_name/PIC_$value.jpg https://loremflickr.com/320/240
   			done
   		zip -re ./$folder_name.zip -P "$uPass" ./$folder_name 
   		;;
   
   		"att")
   
   			awk '
   			BEGIN { print "Ada berapa kali percobaan login?" }
   			/LOGIN/  { ++n }
   			END   { print "Ada", n, "kali percobaan login" }' log.txt
   		;;
   
   		*)
   			echo "There is no such thing"
   		;;
  		esac
 		else 
  			echo "Wrong Password"
  			echo "$TIMESTAMP LOGIN: ERROR Failed login attempt on user $uName" >> $LOGFILE
 		fi
	else echo "User Not Found" 
fi
